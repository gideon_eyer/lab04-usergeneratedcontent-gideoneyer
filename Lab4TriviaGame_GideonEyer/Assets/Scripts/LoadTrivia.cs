﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

//Welp, I tried
//The initial problem is applying a heuristic where the index of a question would access the following two lines as potential answers to the question
//But if the size of our question list is 4 (indexes 0, 1, 2, 3) 
//The size of the potential answers list is double that because there are two answers per question
//So we have 0 to 7 indexes in the answers list which causes problems, because index 2 for the third question
//will point to index 2 in the answers, which is in the middle of completely unrelated answers to the question
//forgive me senpai
public class LoadTrivia : MonoBehaviour
{
    FileInfo originalFile;
    TextAsset textFile;
    TextReader reader;

    public List<string> questions = new List<string>();
    public List<string> answers = new List<string>();
    public List<string> parsedAnswers = new List<string>();

    char delimiterChar = ',';

    void Start ()
    {
        originalFile = new FileInfo(Application.dataPath + "/questions.txt");

        if(originalFile != null && originalFile.Exists)
        {
            reader = originalFile.OpenText();
        }
        else
        {
            textFile = (TextAsset)Resources.Load("embedded", typeof(TextAsset));
            reader = new StringReader(textFile.text);
        }

        string lineOfText;
        int lineNumber = 0;

        while((lineOfText = reader.ReadLine()) != null)
        {

            if(lineNumber % 2 == 0)
            {
                questions.Add(lineOfText);
            }
            else
            {
                //This was where I eventually ended up, trying to split a new format of the embedded text
                //It worked mostly until I realized I'm just recreating the initial problem
                string[] answerText = lineOfText.Split(delimiterChar);
                parsedAnswers.Add(answerText[0]);
                parsedAnswers.Add(answerText[1]);
            }

            //This was pass #1 which loaded the following 2 lines after a question was detected as answers
            //if(lineNumber > 2)
            //{
            //    lineNumber = 0;
            //}

            //if(lineNumber == 0)
            //{
            //    questions.Add(lineOfText);
            //}
            //else
            //{
            //    answers.Add(lineOfText);
            //}

            lineNumber++;
        }

        SendMessage("Begin");
	}

    //void ParseAnswers()
    //{
    //    char delimiterCharacter = ',';
        
    //    for (int i = 0; i < answers.Count + 1; i++)
    //    {
    //        string[] answerToParse = answers[i].Split(delimiterCharacter);
    //        parsedAnswers.Add(answerToParse[i]);
    //    }

    //    SendMessage("Begin");
    //}
}
