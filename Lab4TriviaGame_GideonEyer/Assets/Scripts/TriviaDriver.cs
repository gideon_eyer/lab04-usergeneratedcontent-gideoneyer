﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TriviaDriver : MonoBehaviour
{
    LoadTrivia refs;

    public Text questionText;
    public Text answerA;
    public Text answerB;
    public Text questionsAnswered;
    public Text questionsCorrect;
    public Text questionsIncorrect;

    public Button answerAButton;
    public Button answerBButton;

    string question = "Question woAH";
    string answerAText = "First answer";
    string answerBText = "Second answer";


    int totalAnswered;
    int answeredCorrect;
    int answeredIncorrect;

	// Use this for initialization
	void Begin ()
    {
        refs = gameObject.GetComponent<LoadTrivia>();

        StartGame();
	}

    public void StartGame()
    {
        totalAnswered = 0;
        answeredCorrect = 0;
        answeredIncorrect = 0;

        int numQuestion = refs.questions.Count;
        int randomQuestion = Random.Range(0, numQuestion);

        Debug.Log(numQuestion);
        Debug.Log(randomQuestion);
        Debug.Log(refs.questions[randomQuestion]);

        question = refs.questions[randomQuestion];
        answerA.text = refs.answers[randomQuestion];
        answerB.text = refs.answers[randomQuestion + 1];

        questionText.text = question;
    }

    void ParseAnswers()
    {

    }

    void UpdateText(Text textToUpdate, string textToPut)
    {

    }
}
